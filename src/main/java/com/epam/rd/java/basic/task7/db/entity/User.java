package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

    private int id;
    private String login;

    public User() {
        this.login = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public static User createUser(String login) {
        User user = new User();
        user.setLogin(login);
        return user;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() != obj.getClass()) {
            return false;
        }
        if (obj == null) {
            throw new IllegalArgumentException();
        }
        User user = (User) obj;
        return Objects.equals(this.login, user.getLogin());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return login;
    }

}
