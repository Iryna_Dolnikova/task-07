package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class DBManager {

    private static final String SQL_INSERT_USER = "INSERT INTO users(login) VALUES (?)";
    private static final String SQL_INSERT_TEAM = "INSERT INTO teams(name) VALUES (?)";
    private static final String SQL_INSERT_TEAM_FOR_USER = "INSERT INTO users_teams VALUES (?, ?)";

    private static final String SQL_SELECT_USER = "SELECT * FROM users WHERE login = ?";
    private static final String SQL_SELECT_TEAM = "SELECT * FROM teams WHERE name = ?";
    private static final String SQL_SELECT_USERS = "SELECT * FROM users ORDER BY id";
    private static final String SQL_SELECT_TEAMS = "SELECT * FROM teams ORDER BY id";
    private static final String SQL_SELECT_USER_TEAMS = "SELECT * FROM users_teams JOIN teams "
            + "ON users_teams.team_id = teams.id WHERE user_id =? ";

    private static final String SQL_UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";

    private static final String SQL_DELETE_USER = "DELETE FROM users WHERE login = ?";
    private static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE name = ?";

    private static DBManager dbManager;

    public static synchronized DBManager getInstance() {
        if (dbManager == null) {
            dbManager = new DBManager();
        }
        return dbManager;
    }

    public static Connection getConnection() throws SQLException {
        Properties properties = new Properties();
        try (InputStream inputStream = Files.newInputStream(Paths.get("app.properties"))) {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String connectionUrl = properties.getProperty("connection.url");
        return DriverManager.getConnection(connectionUrl);
    }

    public boolean insertUser(User user) throws DBException {
        boolean result;
        ResultSet resultSet = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, user.getLogin());
            if (preparedStatement.executeUpdate() > 0) {
                resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    int userId = resultSet.getInt(1);
                    user.setId(userId);
                }
            }
            result = true;
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        } finally {
            close(resultSet);
        }
        return result;
    }

    public boolean insertTeam(Team team) throws DBException {
        boolean result;
        ResultSet resultSet = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, team.getName());
            if (preparedStatement.executeUpdate() > 0) {
                resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    int teamId = resultSet.getInt(1);
                    team.setId(teamId);
                }
            }
            result = true;
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        } finally {
            close(resultSet);
        }
        return result;
    }

    public void setTeamsForUser(User user, Team... teams) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(SQL_INSERT_TEAM_FOR_USER);
            for (Team team : teams) {
                preparedStatement.setInt(1, user.getId());
                preparedStatement.setInt(2, team.getId());
                preparedStatement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException ex) {
            rollback(connection);
            throw new DBException(ex.getMessage(), ex.getCause());
        } finally {
            close(connection);
            close(preparedStatement);
        }
    }

    public User getUser(String login) throws DBException {
        User user = null;
        ResultSet resultSet = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER)) {
            preparedStatement.setString(1, login);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = getUserFromResultSet(resultSet);
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        } finally {
            close(resultSet);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;
        ResultSet resultSet = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_TEAM)) {
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                team = getTeamFromResultSet(resultSet);
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        } finally {
            close(resultSet);
        }
        return team;
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_USERS)) {
            while (resultSet.next()) {
                users.add(getUserFromResultSet(resultSet));
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        }
        return users;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_TEAMS)) {
            while (resultSet.next()) {
                teams.add(getTeamFromResultSet(resultSet));
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        }
        return teams;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> userTeams = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_TEAMS)) {
            preparedStatement.setInt(1, user.getId());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                userTeams.add(getTeamFromResultSet(resultSet));
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        } finally {
            close(resultSet);
        }
        return userTeams;
    }

    public void updateTeam(Team team) throws DBException {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_TEAM)) {
            preparedStatement.setString(1, team.getName());
            preparedStatement.setInt(2, team.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        }
    }

    public void deleteTeam(Team team) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_TEAM)) {
            preparedStatement.setString(1, team.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void deleteUsers(User... users) throws DBException {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_USER)) {
            for (User user : users) {
                preparedStatement.setString(1, user.getLogin());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex.getCause());
        }
    }

    private User getUserFromResultSet(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("id"));
        user.setLogin(resultSet.getString("login"));
        return user;
    }

    private Team getTeamFromResultSet(ResultSet resultSet) throws SQLException {
        Team team = new Team();
        team.setId(resultSet.getInt("id"));
        team.setName(resultSet.getString("name"));
        return team;
    }

    private static void rollback(Connection connection) {
        if (connection == null) {
            return;
        }
        try {
            connection.rollback();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private static void close(ResultSet resultSet) {
        if (resultSet == null) {
            return;
        }
        try {
            resultSet.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private static void close(Statement statement) {
        if (statement == null) {
            return;
        }
        try {
            statement.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private static void close(Connection connection) {
        if (connection == null) {
            return;
        }
        try {
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
