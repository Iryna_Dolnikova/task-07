package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class Team {

    private int id;
    private String name;

    public Team() {
        this.name = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Team createTeam(String name) {
        Team team = new Team();
        team.setName(name);
        return team;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() != obj.getClass()) {
            return false;
        }
        if (obj == null) {
            throw new IllegalArgumentException();
        }
        Team team = (Team) obj;
        return Objects.equals(this.name, team.getName());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return name;
    }

}
